$(document).ready(function(){
	$('.js-slider').slick({
		dots: true,
		arrows: false,
		infinite: false,
		speed: 500,
		responsive: [
			{
			breakpoint: 979,
			settings: {
				arrows: true,
				dots: false,
				slidesToShow: 1
				}
			},
			{
			breakpoint: 1180,
			settings: {
				dots: true,
				arrows: false,
				slidesToShow: 1
				}
			}
		]
	});
});


var no = $('.js-lang__link--no')
var en = $('.js-lang__link--en')
var ru = $('.js-lang__link--ru')

no.on('click', function () {
	no.addClass('js-lang__link--active');
	en.removeClass('js-lang__link--active');
	ru.removeClass('js-lang__link--active');
    });
en.on('click', function () {
	en.addClass('js-lang__link--active');
	no.removeClass('js-lang__link--active');
	ru.removeClass('js-lang__link--active');
	});
ru.on('click', function () {
	ru.addClass('js-lang__link--active');
	en.removeClass('js-lang__link--active');
	no.removeClass('js-lang__link--active');
	});